import java.util.Scanner;

public class Zadanie2_4 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe powtorzen n: ");
        int n = in.nextInt();
        double[] tab = new double[n];

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        double min=tab[0];
        double max=tab[0];

        for(int i=1; i<n; i++)
        {
            if(tab[i]<min)
                min=tab[i];
            if(tab[i]>max)
                max=tab[i];
        }
        System.out.print("Najmniejsza: " + min + "\nNajwieksza: " + max);
    }
}
