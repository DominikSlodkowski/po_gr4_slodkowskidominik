import java.util.Scanner;

public class Zadanie1G {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe n (powtorzen): ");
        int n = in.nextInt();
        double wynik1 = 0;
        double wynik2 = 1;
        double[] tab = new double[n];

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        for (int i=0;i<n;i++)
        {
            wynik1 += tab[i];
            wynik2 *= tab[i];
        }

        System.out.println("Wynik 1 to " + wynik1 + "\nWynik 2 to: " + wynik2);
    }
}