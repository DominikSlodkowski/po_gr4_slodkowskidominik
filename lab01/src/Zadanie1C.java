import java.util.Scanner;

public class Zadanie1C {

    public static double abs(double x){
        if(x<0)
            return x*-1;

        return x;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe n (powtorzen): ");
        int n = in.nextInt();
        double wynik = 0;
        double[] tab = new double[n];

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        for (int i=0;i<n;i++)
        {
            wynik += abs(tab[i]);
        }

        System.out.println("Wynik to " + wynik);
    }
}