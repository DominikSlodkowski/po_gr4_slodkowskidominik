import java.util.Scanner;

public class Zadanie1_2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe n (powtorzen): ");
        int n = in.nextInt();
        double[] tab = new double[n];

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        if(n==1)
            System.out.print(" " + tab[0]);
        else
        {
            for (int i=0;i<n;i++) {
                if (n - i < 2)
                    System.out.println(" " + tab[0]);
                else
                    System.out.println(" " + tab[i + 1]);
            }
        }



    }
}