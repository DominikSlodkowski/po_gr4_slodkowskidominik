import java.util.Scanner;

public class Zadanie2_5 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe powtorzen n: ");
        int n = in.nextInt();
        double[] tab = new double[n];
        int l_par=0;

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        for(int i=0; i<n-1; i++)
        {
            if(tab[i]>0&&tab[i+1]>0)
                l_par++;

        }
        System.out.print("Liczba par: " + l_par);
    }
}