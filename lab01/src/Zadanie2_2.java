import java.util.Scanner;

public class Zadanie2_2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe powtorzen n: ");
        int n = in.nextInt();
        double[] tab = new double[n];
        int wynik=0;

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        for(int i=0; i<n; i++)
        {
            if(
                    tab[i]>=0
            )
                wynik+=(tab[i]*2);
        }
        System.out.print("Wynik: " + wynik);
    }
}
