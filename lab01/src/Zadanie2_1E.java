import java.util.Scanner;

public class Zadanie2_1E {

    public static int silnia(int x){
        int wynik = 1;
        for(int i=1; i<=x; i++)
        {
            wynik *= i;
        }
        return wynik;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe powtorzen n: ");
        int n = in.nextInt();
        int[] tab = new int[n];
        int wynik=0;

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe naturalna: ");
            tab[i] = in.nextInt();
        }

        for(int i=0; i<n; i++)
        {
            if(
                    Math.pow(2, i+1)< tab[i] && tab[i]<silnia(i+1)
            )
                wynik++;
        }
        System.out.print("Liczb spelniajacych warunek jest: " + wynik);
    }
}