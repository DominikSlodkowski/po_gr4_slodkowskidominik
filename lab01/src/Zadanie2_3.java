import java.util.Scanner;

public class Zadanie2_3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe powtorzen n: ");
        int n = in.nextInt();
        double[] tab = new double[n];
        int l_dodatnich=0;
        int l_ujemnych=0;
        int l_zer=0;

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        for(int i=0; i<n; i++)
        {
            if(tab[i]>0)
                l_dodatnich++;
            else if(tab[i]<0)
                l_ujemnych++;
            else
                l_zer++;
        }
        System.out.print("Dodatnie: " + l_dodatnich + "\nUjemne: " + l_ujemnych + "\nZera: " + l_zer);
    }
}