import java.util.Scanner;

public class Zadanie1E {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe n (powtorzen): ");
        int n = in.nextInt();
        double wynik = 1;
        double[] tab = new double[n];

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        for (int i=0;i<n;i++)
        {
            wynik *= Math.abs(tab[i]);
        }

        System.out.println("Wynik to " + wynik);
    }
}