import java.util.Scanner;

public class Zadanie1I {

    public static int silnia(int x){
        int wynik = 1;
        for(int i=1; i<=x; i++)
        {
            wynik *= i;
        }
        return wynik;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe n (powtorzen): ");
        int n = in.nextInt();
        double wynik = 0;
        double[] tab = new double[n];

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe rzeczywista: ");
            tab[i] = in.nextDouble();
        }

        for (int i=0;i<n;i++)
        {
            wynik += Math.pow(-1, i+1) * tab[i] / silnia(i+1);
        }

        System.out.println("Wynik to " + wynik);
    }
}