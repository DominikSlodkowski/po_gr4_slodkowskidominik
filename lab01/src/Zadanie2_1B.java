import java.util.Scanner;

public class Zadanie2_1B {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj liczbe powtorzen n: ");
        int n = in.nextInt();
        int[] tab = new int[n];
        int wynik=0;

        for(int i=0; i<n; i++)
        {
            System.out.print("Podaj liczbe naturalna: ");
            tab[i] = in.nextInt();
        }

        for(int i=0; i<n; i++)
        {
            if(tab[i]%3==0&&tab[i]%5!=0)
                wynik++;
        }
        System.out.print("Liczb spelniajacych warunek jest: " + wynik);
    }
}