public class RachunekBankowy {
    static double rocznaStopaProcentowa = 0;
    private double saldo;
    public RachunekBankowy(double newSaldo)
    {
        saldo = newSaldo;
    }
    public void obliczMiesieczneOdsetki(){
        saldo = saldo + ((saldo * rocznaStopaProcentowa)/12);
    }

    public static void setRocznaStopaProcentowa(double newRocznaStopaProcentowa){
        rocznaStopaProcentowa = newRocznaStopaProcentowa;
    }

    public double getSaldo(){
        return saldo;
    }

//    public void setSaldo(double newSaldo){
//        this.saldo = newSaldo;
//    }
}
