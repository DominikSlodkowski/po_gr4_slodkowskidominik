public class testRachunkuBankowego {
    public static void main(String[] args){
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        System.out.println("saver1 0 month saldo : " + saver1.getSaldo());
        System.out.println("saver2 0 month saldo : " + saver2.getSaldo());
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("saver1 1 month saldo : " + saver1.getSaldo());
        System.out.println("saver2 1 month saldo : " + saver2.getSaldo());
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("saver1 2 month saldo : " + saver1.getSaldo());
        System.out.println("saver2 2 month saldo : " + saver2.getSaldo());
    }
}
