public class testIntegerSet {
    public static void main(String[] args){
        IntegerSet integerSet1 = new IntegerSet();
        IntegerSet integerSet2 = new IntegerSet();
        // Test insert, delete, toSting()
        System.out.println(integerSet1);
        System.out.println(integerSet2);
        integerSet1.insertElement(1);
        integerSet1.insertElement(5);
        integerSet1.insertElement(77);
        System.out.println(integerSet1);
        integerSet2.insertElement(2);
        integerSet2.insertElement(7);
        integerSet2.insertElement(100);
        System.out.println(integerSet2);
        integerSet1.deleteElement(1);
        System.out.println(integerSet1);

        //Wypisanie
        System.out.println("Set1 : "); // 5 7 77 100
        integerSet1.insertElement(100);
        integerSet1.insertElement(7);
        System.out.println(integerSet1);
        System.out.println("Set2 : "); // 2 7 100
        System.out.println(integerSet2);

        // Test łączenia zbiorów
        System.out.println("Union : "); // 2 5 7 77 100
        IntegerSet unionSet = IntegerSet.union(integerSet1, integerSet2);
        System.out.println(unionSet);
        System.out.println("Intersection : "); // 7 100
        IntegerSet intersectionSet = IntegerSet.intersection(integerSet1, integerSet2);
        System.out.println(intersectionSet);

        // Test equals
        System.out.println("Set1 : "); // 5 7 77 100
        System.out.println(integerSet1);
        System.out.println("Set2 : "); //2 7 100
        System.out.println(integerSet2);

        System.out.println(integerSet1.equals(integerSet2));

        integerSet1.deleteElement(5);
        integerSet1.deleteElement(77);
        integerSet2.deleteElement(2);

        System.out.println("Set1 : "); // 7 100
        System.out.println(integerSet1);
        System.out.println("Set2 : ");
        System.out.println(integerSet2); // 7 100

        System.out.println(integerSet1.equals(integerSet2));



    }
}
