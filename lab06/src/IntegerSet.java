public class IntegerSet {
    public boolean[] zbior = new boolean[100];

    public IntegerSet()
    {
        for (boolean element: zbior) {element = false;}
    }

    public static IntegerSet union(IntegerSet integerSetA, IntegerSet integerSetB){
        IntegerSet result = new IntegerSet();
        for(int i = 0; i < 100; i++)
        {
            if(integerSetA.zbior[i] || integerSetB.zbior[i])
                result.zbior[i] = true;
        }
        return result;
    }

    public static IntegerSet intersection(IntegerSet integerSetA, IntegerSet integerSetB){
        IntegerSet result = new IntegerSet();
        for(int i = 0; i < 100; i++)
        {
            if(integerSetA.zbior[i] && integerSetB.zbior[i])
                result.zbior[i] = true;
        }
        return result;
    }

    public void insertElement(int liczba){
        zbior[liczba-1] = true;
    }

    public void deleteElement(int liczba){
        zbior[liczba-1] = false;
    }

    public String toString(){
        String result = "";
        for(int i = 0; i < 100; i++)
        {
            if(zbior[i])
                result = result + (i+1) + " ";
        }
        return result;
    }

    public boolean equals(IntegerSet comparedIntegerSet){
        for(int i = 0; i < 100; i++)
        {
            if(zbior[i] != comparedIntegerSet.zbior[i])
                return false;
        }
        return true;
    }
}
