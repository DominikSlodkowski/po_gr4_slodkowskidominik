import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Iterator;

public class Metody {

    public static <T> void redukuj(LinkedList<T> pracownicy, int n){
        int size = pracownicy.size();
        for(int i = n-1, removed_count = 0; i < size; i+=n)
        {
            pracownicy.remove(i-removed_count);
            removed_count+=1;
        }
    }


    public static <T> void odwroc(LinkedList<T> lista){
        for(int i = 0; i < lista.size()/2; i++)
        {
            T temp = lista.get(i);
            lista.set(i, lista.get(lista.size()-i-1));
            lista.set(lista.size()-i-1, temp);
        }
    }

    public static ArrayList<Integer> primes(int n)
    {
        ArrayList<Integer> pierwsze = new ArrayList<Integer>();
        for (int i = 2; i <= n; i++)
            pierwsze.add(i);

        for (int i = 2; i <= n; i++)
        {
            for (int j = 0; j < pierwsze.size(); j++)
            {
                if (pierwsze.get(j)%i == 0 && pierwsze.get(j) != i)
                {
                    pierwsze.remove(j);
                    j--;
                }
            }
        }
        return pierwsze;
    }

    static public <T extends Iterable> void print(T object)
    {
        Iterator<T> iter = object.iterator();
        while(iter.hasNext())
        {
            System.out.print(iter.next());
            if(iter.hasNext())
                System.out.print(", ");
        }
        System.out.println();
    }
}
