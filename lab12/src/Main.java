import java.util.Arrays;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        //Zadanie 1, 2
        LinkedList<Integer> pracownicy = new LinkedList<Integer>(Arrays.asList(
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15));
        Metody.redukuj(pracownicy, 3);
        System.out.println(pracownicy);

        // Zadanie 3, 4
        LinkedList<Integer> lista = new LinkedList<Integer>(Arrays.asList(
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15));
        Metody.odwroc(lista);
        System.out.println(lista);

        // Zadanie 5------------------------------------------------------------------
        Stack<String> stos = new Stack<String>();
        String zdanie = new String("Ala ma kota. Jej kot lubi myszy.");
        StringBuffer sb = new StringBuffer("");
        String wynik = new String("");
        for(int i = 0, j = 0; i < zdanie.length(); i++)
        {
            if(zdanie.charAt(i) == ' ')
            {
                String tmp = zdanie.substring(j, i);
                tmp += " ";
                stos.push(tmp);
                j = i;
            }
            if(zdanie.charAt(i) == '.')
            {
                stos.push(zdanie.substring(j, i));
                j = i;
                while(!stos.isEmpty())
                {
                    sb.append(stos.pop());
                }
                sb.append(".");
            }
        }
        sb.delete(0, 1);
        sb.delete(sb.length()-2, sb.length());
        boolean czy_poczatek = true;
        for(int i = 0; i < sb.length(); i++)
        {

            sb.setCharAt(i, Character.toLowerCase(sb.charAt(i)));
            if(czy_poczatek)
            {
                sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
                czy_poczatek = false;
            }

            if(sb.charAt(i) == '.')
            {
                czy_poczatek = true;
                sb.delete(i-1, i);

            }
        }
        for(int i = 1; i < sb.length(); i++)
        {
            if(sb.charAt(i)==' ' && sb.charAt(i-1) == ' ')
            {
                sb.deleteCharAt(i);
                i--;
            }
        }
        wynik = sb.toString();
        System.out.println(wynik);


        // Zadanie 6-----------------------------------
        Stack<Integer> stos6= new Stack<Integer>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj n: ");
        int a = sc.nextInt();
        if(a==0)
            System.out.println(0);
        while(a!=0)
        {
            stos6.push(a%10);
            a=a/10;
        }
        StringBuffer wynik6 = new StringBuffer();
        while(!stos6.isEmpty())
        {
            wynik6.append(stos6.pop());
            wynik6.append(" ");
        }
        System.out.println(wynik6.toString());

        // Zadanie 7
        System.out.println("Podaj n (do której liczby pierwszej): ");
        int n = sc.nextInt();
        System.out.println(Metody.primes(n));

        // Zadanie 8
        ArrayList<Integer> arrayList = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6));
        Metody.print(arrayList);

        LinkedList<String> linkedList = new LinkedList<String>(Arrays.asList("str1", "str2", "str3"));
        Metody.print(linkedList);
    }
}
