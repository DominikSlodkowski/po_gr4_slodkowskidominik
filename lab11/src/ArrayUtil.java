import java.util.ArrayList;

public class ArrayUtil
{
    static public <T extends Comparable<T>> boolean isSorted(T[] objects)
    {
        for (int i = 0; i < objects.length-1; i++)
        {
            if (objects[i].compareTo(objects[i + 1]) > 0)
            {
                return false;
            }
        }
        return true;
    }

    //zad 4. Zakłada się, że tablica ma wartości już posortowane i nie trzeba tego sprawdzać.
    static public <T extends Comparable<T>> int binSearch(T[] objects, T object)
    {
        int left = 0;
        int right = objects.length - 1;
        int index;

        while (left <= right)
        {
            index = (left + right) / 2;
            if (objects[index].compareTo(object) < 0)
            {
                left = index + 1;
            }
            else if (objects[index].compareTo(object) > 0)
            {
                right = index - 1;
            }
            else
            {
                return index;
            }
        }

        return -1;
    }

    static private <T extends Comparable<T>> int searchMin(int index, T[] tab)
    {
        int result = index;
        T min = tab[index];
        while(index < tab.length)
        {
            int compareResult = tab[index].compareTo(min);
            if(compareResult < 0)
            {
                result = index;
                min = tab[index];
            }
            index ++;
        }
        return result;
    }

    static public <T extends Comparable<T>> void selectionSort(T[] tab)
    {
        for (int i = 0; i < tab.length; i++)
        {
            int index = searchMin(i, tab);
            T temp = tab[i];
            tab[i] = tab[index];
            tab[index] = temp;
        }
    }
    static public <T extends Comparable<T>> void mergeSort(T[] tab){
        ArrayList<T> tabArrayList = new ArrayList<T>();
        for(int i = 0; i< tab.length; i++)
        {
            tabArrayList.add(tab[i]);
        }
        mergeSortArrayList(tabArrayList, tab.length);
        for(int i = 0; i < tab.length; i++){
            tab[i] = tabArrayList.get(i);
        }
    }

    static private <T extends Comparable<T>>  void mergeSortArrayList(ArrayList<T> a, int n)
    {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        ArrayList<T> l = new ArrayList<T>();
        ArrayList<T> r = new ArrayList<T>();
        for(int i = 0; i < mid; i++){
            l.add(a.get(i));
        }
        for(int i = mid; i < n; i++){
            r.add(a.get(i));
        }
        mergeSortArrayList(l, mid);
        mergeSortArrayList(r, n - mid);

        merge(a, l, r, mid, n - mid);

    }

    private static <T extends Comparable<T>> void merge(
            ArrayList<T> a, ArrayList<T> l, ArrayList<T> r, int left, int right)
    {
        int i = 0, j = 0, k = 0;
        while(i < left && j < right){
            if(l.get(i).compareTo(r.get(j)) < 0){
                a.set(k++, l.get(i++));
            }
            else{
                a.set(k++, r.get(j++));
            }
        }
        while(i < left){
            a.set(k++, l.get(i++));
        }
        while (j < right) {
            a.set(k++, r.get(j++));
        }
    }
}
