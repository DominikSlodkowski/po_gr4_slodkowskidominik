public class PairUtilDemo {
    public static void main(String[] args) {
        Pair<String> mm = new Pair("Ala", "ma");
        System.out.println("first = " + mm.getFirst());
        System.out.println("second = " + mm.getSecond());
        PairUtil.swap(mm);
        System.out.println("first = " + mm.getFirst());
        System.out.println("second = " + mm.getSecond());

    }
}
