import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;


public class ArrayUtilMain {
    public static void main(String[] args) {

        Integer[] tabInteger1 = {1, 1, 23, 45, 55, 55}; //Posortowany
        Integer[] tabInteger2 = {4, 3, 25, 2, 154}; // Nieposortowany
        LocalDate d1 = LocalDate.of(2020, 1, 1);
        LocalDate d2 = LocalDate.of(2021, 1, 1);
        LocalDate d3 = LocalDate.of(2021, 1, 1);
        LocalDate d4 = LocalDate.of(2022, 1, 1);
        LocalDate[] tabDate1 = {d1, d2, d3, d4}; //Posortowany
        LocalDate[] tabDate2 = {d4, d3, d2, d1}; // Nieposortowany

        // zadanie 3
        System.out.println(ArrayUtil.isSorted(tabInteger1));
        System.out.println(ArrayUtil.isSorted(tabInteger2));
        System.out.println(ArrayUtil.isSorted(tabDate1));
        System.out.println(ArrayUtil.isSorted(tabDate2));

        // zadanie 4
        System.out.println(ArrayUtil.binSearch(tabInteger1, 23));
        System.out.println(ArrayUtil.binSearch(tabInteger2, 23));
        System.out.println(ArrayUtil.binSearch(tabDate1, LocalDate.of(2020, 1, 1)));
        System.out.println(ArrayUtil.binSearch(tabDate1, LocalDate.of(2100, 1, 1)));

        // zadanie 5
        System.out.println("SelectionSort\n");
        System.out.println(ArrayUtil.isSorted(tabInteger2));
        ArrayUtil.selectionSort(tabInteger2);
        System.out.println(ArrayUtil.isSorted(tabInteger2));

        System.out.println(ArrayUtil.isSorted(tabDate2));
        ArrayUtil.selectionSort(tabDate2);
        System.out.println(ArrayUtil.isSorted(tabDate2));

        // zadanie 6
        System.out.println("MergeSort\n");
        Integer[] tabInteger3 = {4, 3, 25, 2, 154}; // Nieposortowany
        LocalDate[] tabDate3 = {d4, d3, d2, d1}; // Nieposortowany
        System.out.println(ArrayUtil.isSorted(tabInteger3));
        System.out.println(ArrayUtil.isSorted(tabDate3));

        ArrayUtil.mergeSort(tabInteger3);
        ArrayUtil.mergeSort(tabDate3);
        System.out.println(ArrayUtil.isSorted(tabInteger3));
        System.out.println(ArrayUtil.isSorted(tabDate3));
        for(int x: tabInteger3){
            System.out.println(x);
        }

    }

}