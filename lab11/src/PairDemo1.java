public class PairDemo1
{
    public static void main(String[] args)
    {
        Pair<String> mm = new Pair("Ala", "ma");
        System.out.println("first = " + mm.getFirst());
        System.out.println("second = " + mm.getSecond());
        mm.swap();
        System.out.println("first = " + mm.getFirst());
        System.out.println("second = " + mm.getSecond());
    }
}
