import java.util.ArrayList;

public class Zadanie2 {
    public static  void main(String args[]){
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(2);
        a.add(1);
        a.add(15);
        a.add(63);
        a.add(93);
        b.add(3);
        b.add(33);
        b.add(333);
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        wynik = merge(a,b);
        for(int x: wynik)
            System.out.print(x + " ");
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        int size_a = a.size();
        int size_b = b.size();
        int size_max = size_a;
        if(size_max < size_b)
            size_max = size_b;

        for(int i = 0; i<size_max; i++)
        {
            if(i < size_a)
                wynik.add(a.get(i));

            if(i < size_b)
                wynik.add(b.get(i));
        }
        return wynik;
    }
}