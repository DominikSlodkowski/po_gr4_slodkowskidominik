import java.util.ArrayList;

public class Zadanie1 {
    public static void main(String args[]){
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(2);
        a.add(1);
        a.add(15);
        a.add(63);
        b.add(3);
        b.add(33);
        b.add(333);
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        wynik = append(a, b);
        for(int x: wynik)
        {
            System.out.print(x + " ");
        }

    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<>();
        for(int i=0; i<a.size(); i++)
        {
            wynik.add(a.get(i));
        }
        for (int x: b) {
            wynik.add(x);
        }
        return wynik;
    }

}

