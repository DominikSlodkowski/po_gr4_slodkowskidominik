import java.util.ArrayList;

public class Zadanie3 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(15);
        a.add(63);
        a.add(93);
        b.add(3);
        b.add(33);
        b.add(333);
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        wynik = mergeSorted(a,b);
        for(int x: wynik)
            System.out.print(x + " ");

    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for(int i = 0, licznik_a = 0, licznik_b = 0; i < a.size() + b.size(); i++)
        {
            if(licznik_a >= a.size())
            {
                wynik.add(b.get(licznik_b));
                licznik_b++;
            }
            else if(licznik_b >= b.size())
            {
                wynik.add(a.get(licznik_a));
                licznik_a++;
            }
            else if(a.get(licznik_a) < b.get(licznik_b))
            {
                wynik.add(a.get(licznik_a));
                licznik_a++;
            }
            else
            {
                wynik.add(b.get(licznik_b));
                licznik_b++;
            }
        }
        return wynik;
    }
}
// a: 1, 4, 15, 63, 93
// b: 3, 33, 333
// wynik:

