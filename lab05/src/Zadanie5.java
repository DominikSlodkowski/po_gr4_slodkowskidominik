import java.util.ArrayList;

public class Zadanie5 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(15);
        a.add(63);
        a.add(93);
        for(int x: a)
            System.out.print(x + " ");
        reverse(a);
        for(int x: a)
            System.out.print(x + " ");
    }

    public static void reverse(ArrayList<Integer> a){
        int size = a.size();
        for(int i = 0; i < size / 2; i++){
            int tmp_a_number = a.get(i);
            a.set(i, a.get(size- 1 - i));
            a.set(size- 1 - i, tmp_a_number);
        }
    }
}
