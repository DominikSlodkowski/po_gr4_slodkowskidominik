import java.util.ArrayList;

public class Zadanie4 {
    public static void main(String[] main) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(15);
        a.add(63);
        a.add(93);
        ArrayList<Integer> a_reversed = reversed(a);
        System.out.println(reversed_check(a, a_reversed));
        for(int x: a)
            System.out.print(x + " ");
        for(int x: a_reversed)
            System.out.print(x + " ");

    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for(int i = a.size() - 1; i >= 0 ; i--)
        {
            wynik.add(a.get(i));
        }
        return wynik;
    }

    public static boolean reversed_check(ArrayList<Integer> a, ArrayList<Integer> a_reversed) {
        int dlugosc = a.size();
        if(dlugosc!=a_reversed.size())
            return false;
        for(int i = 0; i < dlugosc; i++)
        {
            if(a.get(i) != a_reversed.get(dlugosc - i - 1))
                return false;
        }
        return true;
    }
}
//        zad 4.
//        Napisz dodatkowo funkcję reversed_check(..), która sprawdzi czy metoda reversed działa poprawnie.
//        zad 5.
//        Napisz dodatkowo funkcję reverse_check(..), która sprawdzi czy metoda reverse działa poprawnie.