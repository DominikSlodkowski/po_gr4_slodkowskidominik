package pl.imiajd.slodkowski;
import pl.imiajd.slodkowski.Osoba;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>{
    private double sredniaOcen;

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    @Override
    protected Object clone()throws CloneNotSupportedException{
        return super.clone();
    }

    @Override
    public int compareTo(Osoba os) {
        int result = super.compareTo(os);
        if (result == 0)
            result = Double.compare(getSredniaOcen(), ((Student)os).getSredniaOcen());
        return result;
    }

    @Override
    public String toString(){
        return "Student["+ getNazwisko() + ", " + getDataUrodzenia() + ", " + getSredniaOcen() + "]";
    }
}
