import pl.imiajd.slodkowski.Student;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<pl.imiajd.slodkowski.Student> grupa = new ArrayList<Student>();

        Student s1 = new Student("Słodkowski", LocalDate.of(2001, 1, 1), 4.45);
        grupa.add(s1);

        Student s2 = new Student("Kowal", LocalDate.of(2000, 11, 1), 4.2);
        grupa.add(s2);

        Student s3 = new Student("Kowal", LocalDate.of(2000, 11, 1), 3.05);
        grupa.add(s3);

        Student s4 = new Student("Kwiek", LocalDate.of(2000, 4, 15), 4.66);
        grupa.add(s4);

        Student s5 = new Student("Kwitek", LocalDate.of(2000, 4, 15), 4.66);
        grupa.add(s5);

        for (Student s: grupa) {
            System.out.println(s);
        }
        grupa.sort(Student::compareTo);

        System.out.println("\n");

        for (Student s: grupa) {
            System.out.println(s);
        }

    }
}
