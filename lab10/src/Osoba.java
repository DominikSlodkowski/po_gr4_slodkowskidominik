package pl.imiajd.slodkowski;


import java.time.LocalDate;


public class Osoba implements Cloneable, Comparable<Osoba>{
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba(String nazwisko, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    @Override
    public String toString(){
        return "Osoba["+ getNazwisko() + ", " + getDataUrodzenia() + "]";
    }

    @Override
    public boolean equals(Object os) {
        if(this == os)
            return true;
        if(os == null || getClass() != os.getClass())
            return false;
        Osoba comparedOs = (Osoba)os;
        return getNazwisko().equals(comparedOs.getNazwisko()) &&
                getDataUrodzenia().equals(comparedOs.getDataUrodzenia());
    }

    @Override
    public int compareTo(Osoba os) {
        int result = getNazwisko().compareTo(os.getNazwisko());
        if (result == 0)
            result = getDataUrodzenia().compareTo(os.getDataUrodzenia());
        return result > 0 ? 1 : (result == 0 ? 0 : -1);
    }

    @Override
    protected Object clone()throws CloneNotSupportedException{
        return super.clone();
    }

}
