import pl.imiajd.slodkowski.Osoba;

import java.time.LocalDate;
import java.util.ArrayList;


public class TestOsoba {
    public static void main(String[] args){
        ArrayList<Osoba> grupa = new ArrayList<Osoba>();

        Osoba o1 = new Osoba("Słodkowski", LocalDate.of(2001, 1, 1));
        grupa.add(o1);

        Osoba o2 = new Osoba("Kowalski", LocalDate.of(1990, 12, 21));
        grupa.add(o2);

        Osoba o3 = new Osoba("Kowalski", LocalDate.of(1989, 12, 21));
        grupa.add(o3);

        Osoba o4 = new Osoba("Nowak", LocalDate.of(1986, 6, 14));
        grupa.add(o4);

        Osoba o5 = new Osoba("Kowal", LocalDate.of(1986, 6, 14));
        grupa.add(o5);

        for (Osoba os: grupa) {
            System.out.println(os);
        }
        grupa.sort(Osoba::compareTo);

        System.out.println("\n");

        for (Osoba os: grupa) {
            System.out.println(os);
        }
    }
}
