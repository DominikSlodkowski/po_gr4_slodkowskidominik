import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Program3 {
    public static void main(String[] args) {

        try
        {
            BufferedReader bf = new BufferedReader(new FileReader(args[0]));
            ArrayList<String> result = new ArrayList<String>();
            String line = bf.readLine();
            while(line != null)
            {
                result.add(line);
                line = bf.readLine();
            }
            bf.close();

            System.out.println(result);
            result.sort(String::compareTo);
            System.out.println(result);
        }
        catch (Exception e)
        {
            return;
        }
    }

}
