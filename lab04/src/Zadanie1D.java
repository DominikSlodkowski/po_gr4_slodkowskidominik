import java.util.Scanner;

public class Zadanie1D {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj str do powtorzenia: ");
        String str = in.next();
        System.out.println("Podaj liczbe powtorzen: ");
        int n = in.nextInt();
        System.out.println(repeat(str, n));
    }
    public static String repeat(String str,int n){
        String result = "";
        for(int i=0; i<n; i++)
        {
               result = result.concat(str);
        }
        return result;
    }
}
