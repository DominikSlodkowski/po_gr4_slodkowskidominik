import java.util.Scanner;

public class Zadanie1A {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj string");
        String str = in.next();
        System.out.println("Podaj char");
        char c = in.next().charAt(0);
        System.out.println("Ilosc powtorzen char:  " + countChar(str, c));


    }
    public static int countChar(String str, char c){
        int wynik = 0;
        for(int i=0; i<str.length(); i++)
        {
            if (str.charAt(i)==c)
                wynik++;
        }
        return wynik;
    }
}
