import java.math.BigDecimal;
import java.math.RoundingMode;

public class Zadanie5 {
    public static void main(String[] args){
    System.out.println(iloscKapitaluPoLatach(100, 0.01, 1000));
    }

    public static BigDecimal iloscKapitaluPoLatach(int k, double p, int n){
    BigDecimal bd = new BigDecimal(k);
    BigDecimal procent = new BigDecimal(1 + p);
    for(int i = 0; i < n; i++)
    {
        bd = bd.multiply(procent);
    }

    bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd;
    }
}
