import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

public class Zadanie2 {
    public static void main(String[] args) throws Exception{
        // pass the path to the file as a parameter
        String sciezka = "D:\\STUDIA\\PO\\zadanie2.txt";
        char znak = 'a';
        System. out.println("ilosc wystapien znaku " + znak + " w teksie: " + liczenieZnakuPliku(sciezka, znak));
    }

    public static int liczenieZnakuPliku(String sciezka, char znak) throws Exception{
        BufferedReader br = new BufferedReader(new FileReader(new File(sciezka)));
        String line;
        int wynik = 0;
        while ((line = br.readLine()) != null) {
            for(int i = 0; i < line.length(); i++)
            {
                if(line.charAt(i) == znak)
                    wynik++;
            }
        }
        br.close();
        return wynik;
    }


}
