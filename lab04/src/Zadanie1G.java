import java.util.Scanner;
import java.lang.StringBuffer;

public class Zadanie1G {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("podaj liczbe: ");
        String str = sc.next();
        System.out.println("Zmodyfikowany: " + nice(str));
    }

    public static String nice(String str){
        StringBuffer sb = new StringBuffer();
        StringBuffer sb2 = new StringBuffer();
        String wynik;
        sb.append(str);
        sb.reverse();
        int licznik = 0;
        for(int i = 0; i < str.length(); i++)
        {
            if(licznik == 3)
            {
                licznik = 0;
                sb2.append("'");
            }
            licznik++;
            sb2.append(sb.charAt(i));
        }
        sb2.reverse();
        wynik = sb2.toString();
        return wynik;
    }
}
