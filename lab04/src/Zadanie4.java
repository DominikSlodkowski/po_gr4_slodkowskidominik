import java.math.BigInteger;

public class Zadanie4 {
    public static void main(String args[]){
        int n = 5;
    System.out.println("Liczba ziarenek tablicy o wymiaze " + n + "x" + n + ": ");
    System.out.println(ileZiarenek(n).toString());
    }

    public static BigInteger ileZiarenek(int n){
        BigInteger wynik = new BigInteger("1");
        BigInteger tmp = wynik;
        for(int i = 1; i < n * n; i++)
        {
            tmp = tmp.add(tmp);
            wynik = wynik.add(tmp);
        }
        return wynik;
    }
}
