import java.util.Scanner;

public class Zadanie1C {
    public static void main(String args[])
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj str");
        String str = in.next();
        System.out.println("Middle: " + middle(str));
    }

    public static String middle(String str){
        if(str.length()%2==1)
        {
            char c =  str.charAt(str.length()/2);
            return String.valueOf(c);
        }
        return str.substring(str.length()/2-1, str.length()/2+1);
    }
}
