import java.util.Scanner;
import java.lang.StringBuffer;

public class Zadanie1F {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj string");
        String str = in.next();
        System.out.println(change(str));

    }

    public static String change(String str){
        StringBuffer sb = new StringBuffer();
        String wynik;
        char[] tmp = str.toCharArray();
        for(int i = 0; i < tmp.length; i++)
        {
            if(Character.isUpperCase(tmp[i]))
                tmp[i] = Character.toLowerCase(tmp[i]);
            else if(Character.isLowerCase(tmp[i]))
                tmp[i] = Character.toUpperCase(tmp[i]);
        }
        for(int i = 0; i < tmp.length; i++)
        {
            sb.append(tmp[i]);
        }
        wynik = sb.toString();
        return wynik;
    }
}
