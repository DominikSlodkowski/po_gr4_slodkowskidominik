import javax.xml.stream.FactoryConfigurationError;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

public class Zadanie3 {
    public static void main(String[] args) throws Exception{
        // pass the path to the file as a parameter
        String sciezka = "D:\\STUDIA\\PO\\zadanie2.txt";
        String wyraz = "linia";
        System. out.println("ilosc wystapien wyrazu " + wyraz + " w teksie: " + liczenieWyrazuPliku(sciezka, wyraz));
    }

    public static int liczenieWyrazuPliku(String sciezka, String wyraz) throws Exception{
        BufferedReader br = new BufferedReader(new FileReader(new File(sciezka)));
        String line;
        int wynik = 0;
        while ((line = br.readLine()) != null) {
            for(int i = 0; i < line.length() - wyraz.length(); i++)
            {
                boolean czy_wynik_plus = false;
                for(int j = 0; j < wyraz.length(); j++)
                {
                    czy_wynik_plus = true;
                    if(wyraz.charAt(j) != line.charAt(i + j))
                        czy_wynik_plus = false;
                        break;
                }
                if(czy_wynik_plus)
                    wynik++;
            }
        }
        br.close();
        return wynik;
    }


}
