import java.util.Random;
import java.util.Scanner;

public class Zadanie1D {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n <1; 100> : ");
        int n = in.nextInt();

        if(n>100||n<1) {
            System.out.println("Podano złe n");
            return;
        }


        int[] tab = new int[n];
        // losowanie n liczb
        Random rand = new Random();
        for(int i=0;i<n;i++)
        {
            tab[i] = rand.nextInt(2000) - 999;
        }

        int suma_ujemnych = 0;
        int suma_dodatnich = 0;

        for(int i=0;i<n;i++)
        {
            if(tab[i] < 0)
                suma_ujemnych += tab[i];
            else
                suma_dodatnich += tab[i];
        }

        System.out.print("Suma dodatnich:  " + suma_dodatnich + "\tSuma ujemnych: " + suma_ujemnych);
    }
}