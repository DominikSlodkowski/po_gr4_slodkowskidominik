import java.util.Random;
import java.util.Scanner;

public class Zadanie1B {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n <1; 100> : ");
        int n = in.nextInt();

        if(n>100||n<1) {
            System.out.println("Podano złe n");
            return;
        }


        int[] tab = new int[n];
        // losowanie n liczb
        Random rand = new Random();
        for(int i=0;i<n;i++)
        {
            tab[i] = rand.nextInt(2000) - 999;
        }

        int l_dodatchich = 0;
        int l_ujemnych = 0;
        int l_zer = 0;

        for(int i=0;i<n;i++)
        {
            if(tab[i] == 0)
                l_zer ++;
            if(tab[i]>0)
                l_dodatchich ++;
            else
                l_ujemnych ++;
        }

        System.out.print("Jest " + l_zer + " zer,  " + l_dodatchich + " liczb dodatnich oraz " +
                l_ujemnych + " liczb ujemnych");
    }
}