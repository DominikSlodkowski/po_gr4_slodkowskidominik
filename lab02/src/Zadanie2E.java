import java.util.Random;
import java.util.Scanner;

public class Zadanie2E {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n<1;100>: ");
        int n = in.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        System.out.println("Najdluzszy ciag dodatnich: : " + dlugoscMaksymalnegoCiaguDodatnich(tab));
//        for(int i =0;i<n;i++)
//            System.out.println(tab[i]);
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich (int tab[]){
        int max_dlugosc = 0;
        int dlugosc = 0;

        for(int i=0;i<tab.length;i++)
        {
            if(tab[i] > 0)
            {
                dlugosc ++;
            }
            if(tab[i] <= 0 || i == tab.length-1)
            {
                if(max_dlugosc < dlugosc)
                    max_dlugosc = dlugosc;
                dlugosc = 0;
            }
        }
        return max_dlugosc;
    }

}
