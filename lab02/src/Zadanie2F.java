import java.util.Random;
import java.util.Scanner;

public class Zadanie2F {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n<1;100>: ");
        int n = in.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        signum(tab);
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static void signum(int tab[]){
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i] > 0)
                tab[i] = 1;
            else if(tab[i] < 0)
                tab[i] = -1;
        }

        for(int i=0; i<tab.length; i++)
        {
            System.out.println(tab[i]);
        }
    }

}
