import java.util.Random;
import java.util.Scanner;

public class Zadanie2D {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n<1;100>: ");
        int n = in.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        System.out.println("suma dodatnich: : " + sumaDodatnich(tab));
        System.out.println("suma ujemnych: : " + sumaUjemnych(tab));
//        for(int i =0;i<n;i++)
//            System.out.println(tab[i]);
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static int sumaDodatnich (int tab[]){
        int suma_dodatnich = 0;

        for(int i=0;i<tab.length;i++)
        {
            if(tab[i] > 0)
                suma_dodatnich += tab[i];
        }
        return suma_dodatnich;
    }

    public static int sumaUjemnych (int tab[]){
        int suma_ujemnych = 0;

        for(int i=0;i<tab.length;i++)
        {
            if(tab[i] < 0)
                suma_ujemnych += tab[i];
        }
        return suma_ujemnych;
    }

}
