import java.util.Random;
import java.util.Scanner;

public class Zadanie1A {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n <1; 100> : ");
        int n = in.nextInt();

        if (n > 100 || n < 1) {
            System.out.println("Podano złe n");
            return;
        }


        int[] tab = new int[n];
        // losowanie n liczb
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(2000) - 999;
        }

        int l_nieparzystych = 0;
        int l_parzystych = 0;

        for (int i = 0; i < n; i++) {
            if (tab[i] % 2 == 0)
                l_parzystych += 1;
            else
                l_nieparzystych += 1;
        }
        System.out.print("Jest " + l_parzystych + " parzystych liczb i " + l_nieparzystych + " liczb nieparzystych");


    }
}

