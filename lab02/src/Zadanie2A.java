import java.util.Random;
import java.util.Scanner;

public class Zadanie2A {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n<1;100>: ");
        int n = in.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        System.out.println("Liczba Nieparzystych: " + ileNieparzystych(tab));
        System.out.println("Liczba parzystych: " + ileParzystych(tab));
//        for(int i =0;i<n;i++)
//            System.out.println(tab[i]);
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static int ileNieparzystych (int tab[]){
        int l = 0;
        for (int i=0; i<tab.length; i++) {
            if (tab[i] % 2 != 0)
                l++;
        }
        return l;
    }

    public static int ileParzystych (int tab[]){
        int l = 0;
        for (int i=0; i<tab.length; i++) {
            if (tab[i] % 2 == 0)
                l++;
        }
        return l;
    }

}
