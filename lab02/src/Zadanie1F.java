import java.util.Random;
import java.util.Scanner;

public class Zadanie1F {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n <1; 100> : ");
        int n = in.nextInt();

        if(n>100||n<1) {
            System.out.println("Podano złe n");
            return;
        }


        int[] tab = new int[n];
        // losowanie n liczb
        Random rand = new Random();
        for(int i=0;i<n;i++)
        {
            tab[i] = rand.nextInt(2000) - 999;
        }


        for(int i=0;i<n;i++)
        {
            if(tab[i] > 0)
                tab[i] = 1;
            else if(tab[i] < 0)
                tab[i] = -1;
        }

        for(int i=0; i<n; i++)
        {
            System.out.println(tab[i]);
        }
    }
}