import java.util.Random;
import java.util.Scanner;

public class Zadanie2G {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n<5;100>(program odwroci pozycje 1<=n<=5): ");
        int n = in.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        for(int i =0;i<n;i++)
            System.out.println(tab[i]);
        odwrocFragment(tab, 1, 5);
        for(int i =0;i<n;i++)
            System.out.println(tab[i]);
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static void odwrocFragment(int tab[], int lewy, int prawy){
        lewy--;
        prawy--;
        int k = prawy - (prawy-lewy)/2;
        for(int i=lewy;i<=k;i++)
        {
            int tmp = tab[i];
            tab[i] = tab[prawy-i+lewy];
            tab[prawy-i+lewy] = tmp;
        }
    }

}
