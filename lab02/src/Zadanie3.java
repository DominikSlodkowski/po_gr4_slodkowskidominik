import java.util.Scanner;
import java.util.Random;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj m: ");
        int m = in.nextInt();
        System.out.print("Podaj n: ");
        int n = in.nextInt();
        System.out.print("Podaj k: ");
        int k = in.nextInt();

        int[][] a = new int[m][n];
        int[][] b = new int[n][k];
        int[][] c = new int[m][k];

        Random rand = new Random();

        // Wypełnienie a
        for(int i=0; i<m; i++)
        {
            for(int j=0; j<n; j++)
            {
                a[i][j]=rand.nextInt(10);
            }
        }

        // Wypełnienie b
        for(int i=0; i<n; i++)
        {
            for(int j=0; j<k; j++)
            {
                b[i][j]=rand.nextInt(10);
            }
        }

        // Mnozenie a*b
        for(int i=0; i<m; i++)
        {
            for(int j=0; j<k; j++)
            {
                int wynik = 0;
                for(int i1=0; i1<n;i1++)
                {
                    wynik += a[i][i1] * b[i1][j];
                }
                c[i][j] = wynik;
            }
        }

        // Wypisanie c
        for(int i=0; i<m;i++)
        {
            for(int j=0; j<k; j++)
            {
                System.out.print(c[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
