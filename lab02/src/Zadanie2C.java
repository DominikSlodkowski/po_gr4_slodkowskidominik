import java.util.Random;
import java.util.Scanner;

public class Zadanie2C {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n<1;100>: ");
        int n = in.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        System.out.println("Liczba Maksymalnych: " + ileMaksymalnych(tab));
//        for(int i =0;i<n;i++)
//            System.out.println(tab[i]);
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static int ileMaksymalnych (int tab[]){
        int max = tab[0];
        int l_max = 1;
        for(int i=1;i<tab.length;i++)
        {
            if(tab[i] == max)
                l_max ++;
            if(tab[i]>max)
            {
                max = tab[i];
                l_max = 1;
            }
        }
        return l_max;
    }

}
