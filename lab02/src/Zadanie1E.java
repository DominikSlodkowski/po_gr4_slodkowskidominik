import java.util.Random;
import java.util.Scanner;

public class Zadanie1E {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Podaj n <1; 100> : ");
        int n = in.nextInt();

        if(n>100||n<1) {
            System.out.println("Podano złe n");
            return;
        }


        int[] tab = new int[n];
        // losowanie n liczb
        Random rand = new Random();
        for(int i=0;i<n;i++)
        {
            tab[i] = rand.nextInt(2000) - 999;
        }

        int max_dlugosc = 0;
        int dlugosc = 0;

        for(int i=0;i<n;i++)
        {
            if(tab[i] > 0)
            {
                dlugosc ++;
            }
            else if(tab[i] <= 0 || i == n-1)
            {
                if(max_dlugosc < dlugosc)
                    max_dlugosc = dlugosc;
                dlugosc = 0;
            }

        }

        System.out.print("Najdluzszy fragment liczb dodatnich, dlugosc:  " + max_dlugosc);
    }
}