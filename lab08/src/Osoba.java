package pl.imiajd.slodkowski;

import java.time.LocalDate;

public class Osoba {

    private String nazwisko;
    private int rokUrodzenia;
    private LocalDate dataUrodzenia;
    private boolean plec;
    private String[] imiona;

    public Osoba(String nazwisko, int rokUrodzenia, LocalDate dataUrodzenia, boolean plec, String[] imiona) {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
        this.imiona = imiona;
    }

    public String getNazwisko() {
        return this.nazwisko;
    }

    public int getRokUrodzenia() {
        return this.rokUrodzenia;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public boolean getPlec() {
        return plec;
    }

    public String[] getImiona() {
        return imiona;
    }


    @Override public String toString() {
        String result = "Nazwisko: " + getNazwisko() + ", Rok urodzenia: " + getRokUrodzenia() + ", data urodzenia: " +
                getDataUrodzenia() + ", plec: " + getPlec() + ", Imiona: ";
        for(String imie: this.getImiona()){
            result = result + imie + ", ";
        }
        return result;
    }
}