package pl.imiajd.slodkowski;
import pl.imiajd.slodkowski.Osoba;

import java.time.LocalDate;

public class Student extends Osoba{
    private String kierunek;
    private double sredniaOcen;

    public Student(String nazwisko, int rokUrodzenia, String kierunek,
                   LocalDate dataUrodzenia, boolean plec, String[] imiona, double sredniaOcen) {
        super(nazwisko, rokUrodzenia, dataUrodzenia, plec, imiona);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getKierunek(){
        return kierunek;
    }

    public double getSredniaOcen(){
        return sredniaOcen;
    }

    public void setSredniaOcen(double newSredniaOcen){
        this.sredniaOcen = newSredniaOcen;
    }

    @Override public String toString(){
        return super.toString() + ", Kierunek: " + getKierunek() + ", Srednia ocen: " + getSredniaOcen();
    }
}