package pl.imiajd.slodkowski;
import pl.imiajd.slodkowski.Osoba;

import java.time.LocalDate;

public class Pracownik extends Osoba{
    private double pensja;
    private LocalDate dataZatrudnienia;

    public Pracownik(String nazwisko, int rokUrodzenia, double pensja,
                     LocalDate dataUrodzenia, boolean plec, String[] imiona, LocalDate dataZatrudnienia){
        super(nazwisko, rokUrodzenia, dataUrodzenia, plec, imiona);
        this.pensja = pensja;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPensja() {
        return this.pensja;
    }

    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }

    @Override public String toString(){
        return super.toString() + ", Pensja: " + getPensja() + ", Data zatrudnienia: " + getDataZatrudnienia();
    }
}
