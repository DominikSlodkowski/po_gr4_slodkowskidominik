package pl.imiajd.slodkowski;

import java.time.LocalDate;
import pl.imiajd.slodkowski.Instrument;

public class Skrzypce extends Instrument {

    public Skrzypce(String producent, LocalDate rokProdukcji){
        super(producent, rokProdukcji);
    }

    public void dzwiek(){
        System.out.println("Skrzzzyyyyyp");
    }

    @Override public String toString(){
        return "Skrzypce, " + super.toString();
    }
}
