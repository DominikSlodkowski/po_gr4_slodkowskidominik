package pl.imiajd.slodkowski;

import pl.imiajd.slodkowski.Osoba;
import pl.imiajd.slodkowski.Pracownik;
import pl.imiajd.slodkowski.Student;
import java.time.LocalDate;

public class TestOsoba {
    public static void main(String[] args){
        String[] imiona1 = {"Jan", "Maria"};
        Osoba osoba1 = new Osoba("Kowalski", 2001, LocalDate.of(2001,1,1),
                true, imiona1);
        System.out.println(osoba1);

        String[] imiona2 = {"Stefan"};
        Pracownik pracownik1 = new Pracownik("Burczymucha",1980, 4000.25,
                LocalDate.of(1980, 4,7),true, imiona2,
                LocalDate.of(2001,3,4));

        System.out.println(pracownik1);
        System.out.println(pracownik1.getPensja());

        String[] imiona3 = {"Jacek", "Antoni", "Grzegorz", "Andrzej", "Inigo"};

        Student student1 = new Student("Montoya", 2000, "Informatyka",
                LocalDate.of(2000, 8, 19), true, imiona3, 4.75);
        System.out.println(student1);
        System.out.println(student1.getSredniaOcen());
        student1.setSredniaOcen(3.23);
        System.out.println(student1.getSredniaOcen());
    }
}
