package pl.imiajd.slodkowski;

import java.time.LocalDate;
import pl.imiajd.slodkowski.Instrument;

public class Flet extends Instrument {

    public Flet(String producent, LocalDate rokProdukcji){
        super(producent, rokProdukcji);
    }

    public void dzwiek(){
        System.out.println("Fiiiiiiiiii");
    }

    @Override public String toString(){
        return "Flet, " + super.toString();
    }
}
