package pl.imiajd.slodkowski;

import java.time.LocalDate;


public abstract class Instrument {
    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    public abstract void dzwiek();

    @Override public String toString(){
        return "Producent: " + getProducent() + ", rok produkcji: " + getRokProdukcji();
    }

    public boolean equals(Instrument comparedInstrument){
        if(this.getClass() != comparedInstrument.getClass())
            return false;
        if(this.getProducent() == comparedInstrument.getProducent())
            if(this.getRokProdukcji().equals(comparedInstrument.getRokProdukcji()))
                return true;
        return false;
    }
}
