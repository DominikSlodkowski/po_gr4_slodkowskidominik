package pl.imiajd.slodkowski;

import java.time.LocalDate;
import pl.imiajd.slodkowski.Instrument;

public class Fortepian extends Instrument {

    public Fortepian(String producent, LocalDate rokProdukcji){
        super(producent, rokProdukcji);
    }

    public void dzwiek(){
        System.out.println("Dan dan daaaannn");
    }

    @Override public String toString(){
        return "Fortepian, " + super.toString();
    }
}