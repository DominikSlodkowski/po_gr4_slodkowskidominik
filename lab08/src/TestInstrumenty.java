import pl.imiajd.slodkowski.Instrument;
import pl.imiajd.slodkowski.Flet;
import pl.imiajd.slodkowski.Fortepian;
import pl.imiajd.slodkowski.Skrzypce;
import java.time.LocalDate;
import java.util.Random;

public class TestInstrumenty {
    public static void main(String[] args){
        Random rand = new Random();
        Instrument[] orkiestra = new Instrument[5];
        Flet flet1 = new Flet("Pracownia tradycyjna", LocalDate.of(2000, 2, 14));
        orkiestra[0] = flet1;
        Flet flet2 = new Flet("Pracownia tradycyjna", LocalDate.of(2000, 2, 14));
        orkiestra[1] = flet2;
        Fortepian fortepian1 = new Fortepian("Fletnisko", LocalDate.of(2020, 1, 8));
        orkiestra[2] = fortepian1;
        Skrzypce skrz1 = new Skrzypce("Pracownia tradycyjna", LocalDate.of(2000, 2, 14));
        orkiestra[3] = skrz1;
        Skrzypce skrz2 = new Skrzypce("Pracownia lutnicza", LocalDate.of(1990, 4, 24));
        orkiestra[4] = skrz2;

        //koncert
        for(int i = 0; i < 15; i++){
            int n = rand.nextInt(5);
            orkiestra[n].dzwiek();
        }

        //test equals
        System.out.println(flet2.equals(skrz1));
        System.out.println(flet2.equals(flet1));

        //print orkiestry
        for(Instrument x: orkiestra){
            System.out.println(x);
        }
    }
}
