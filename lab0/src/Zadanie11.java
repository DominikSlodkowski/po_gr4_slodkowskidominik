public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("\n" +
                "\n" +
                "Tu leży staroświecka jak przecinek\n" +
                "\n" +
                "autorka paru wierszy. Wieczny odpoczynek\n" +
                "\n" +
                "raczyła dać jej ziemia, pomimo że trup\n" +
                "\n" +
                "nie należał do żadnej z literackich grup.\n" +
                "\n" +
                "Ale tez nic lepszego nie ma na mogile\n" +
                "\n" +
                "oprócz tej rymowanki, łopianu i sowy.\n" +
                "\n" +
                "Przechodniu, wyjmij z teczki mózg elektronowy\n" +
                "\n" +
                "i nad losem Szymborskiej podumaj przez chwilę.\n");
    }
}
