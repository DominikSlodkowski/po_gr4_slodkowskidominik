import java.text.DecimalFormat;

public class Zadanie4 {
    private static final DecimalFormat df2 = new DecimalFormat("#.##");

    public static void main(String[] args) {
        double saldo = 1000;
        for(int i=1; i<=3; i++)
        {
            saldo *= 1.06;
            System.out.println("Saldo konta po " + i + " roku wynosi " + df2.format(saldo) + "zl");

            // Rozwiazanie lepsze bez dodatkowego importu:
            // System.out.println("Saldo konta po " + i + " roku wynosi " + String.format("%.2f", saldo) + "zl");
        }
    }
}
