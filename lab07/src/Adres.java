package pl.imiajd.slodkowski;

public class Adres {
    private String ulica;
    private String numer_domu;
    private String numer_mieszkania = null;
    private String miasto;
    private int kod_pocztowy_1;
    private int kod_pocztowy_2;

    public Adres(String ulica, String numer_domu, String numer_mieszkania,
                 String miasto, int kod_pocztowy_1, int kod_pocztowy_2)
    {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy_1 = kod_pocztowy_1;
        this.kod_pocztowy_2 = kod_pocztowy_2;
    }

    public Adres(String ulica, String numer_domu,
                 String miasto, int kod_pocztowy_1, int kod_pocztowy_2)
    {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy_1 = kod_pocztowy_1;
        this.kod_pocztowy_2 = kod_pocztowy_2;
    }

    public void pokaz()
    {
        System.out.println(this.kod_pocztowy_1 + "-" +this.kod_pocztowy_2 + " " + this.miasto);
        if(this.numer_mieszkania != null)
            System.out.println(this.ulica + " " + this.numer_domu + "/" + this.numer_mieszkania);
        else
            System.out.println(this.ulica + " " + this.numer_domu);
    }

    public boolean przed(Adres comparedAdres)
    {
        if(this.kod_pocztowy_1 < comparedAdres.kod_pocztowy_1)
            return true;
        else if(this.kod_pocztowy_1 == comparedAdres.kod_pocztowy_1)
            if(this.kod_pocztowy_2 < comparedAdres.kod_pocztowy_2)
                return true;
        return false;
    }
}
