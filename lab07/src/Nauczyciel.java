package pl.imiajd.slodkowski;
import pl.imiajd.slodkowski.Osoba;

public class Nauczyciel extends Osoba{
    private double pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public double getPensja() {
        return this.pensja;
    }

    @Override public String toString(){
        return super.toString() + ", Pensja: " + getPensja();
    }
}
