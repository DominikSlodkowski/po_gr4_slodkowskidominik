package pl.imiajd.slodkowski;
import java.awt.Rectangle;

public class BetterRectangle extends Rectangle{
    public BetterRectangle(int height, int width, int x, int y){
//        super.setLocation(x, y);
//        super.setSize(width, height);
        super(x, y, width, height);
    }

    public int getPerimeter(){
        return super.height * 2 + super.width * 2;
    }

    public int getArea(){
        return super.height * super.width;
    }
}
