// package pl.imiajd.slodkowski;
import pl.imiajd.slodkowski.Adres;
import pl.imiajd.slodkowski.Osoba;
import pl.imiajd.slodkowski.Student;
import pl.imiajd.slodkowski.Nauczyciel;
import pl.imiajd.slodkowski.BetterRectangle;


public class Main {
    public static void main(String[] args)
    {
        //Test Adres
        Adres adres1 = new Adres("Starzynskiego",
                "4", "6", "Bartoszyce", 11, 200);
        adres1.pokaz();

        Adres adres2 = new Adres("Starzynskiego",
                "4", "Bartoszyce", 11, 201);
        adres2.pokaz();
        System.out.println(adres1.przed(adres2) + "\n");

        // Test Osoba
        Osoba osoba1 = new Osoba("Kowalski", 1990);
        System.out.println(osoba1);
        System.out.println(osoba1.getNazwisko());
        System.out.println(osoba1.getRokUrodzenia() + "\n");

        // Test Student
        Student student1 = new Student("Dratewka", 1991, "Informatyka");
        System.out.println(student1);
        System.out.println(student1.getNazwisko());
        System.out.println(student1.getRokUrodzenia());
        System.out.println(student1.getKierunek() + "\n");

        // Test Nauczyciel
        Nauczyciel nauczyciel1 = new Nauczyciel("Kowalska", 1978, 8000.06);
        System.out.println(nauczyciel1);
        System.out.println(nauczyciel1.getNazwisko());
        System.out.println(nauczyciel1.getRokUrodzenia());
        System.out.println(nauczyciel1.getPensja() + "\n");

        // Test BetterRectangle
        BetterRectangle betterRectangle1= new BetterRectangle(6, 4, 0, 0);
        System.out.println(betterRectangle1.getArea());
        System.out.println(betterRectangle1.getPerimeter());

    }
}